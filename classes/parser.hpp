#ifndef __PARSER_
#define __PARSER__
#include <classRoom.hpp>
#include <exam.hpp>
#include <string>
#include <vector>

class parser
{
public:
  parser () {}

  std::vector<exam *> parseExams (const std::string &fileName);
  std::vector<classRoom *> parseClassRooms (const std::string &fileName);
  static std::vector<std::string> split (const std::string &s, char delim);
  std::string parseDate (const std::string &fileName);
};
#endif
