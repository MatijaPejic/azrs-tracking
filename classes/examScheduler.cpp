#include "examScheduler.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>

auto
comparator (const exam *lhs, const exam *rhs) -> bool
{
  return static_cast<int> (lhs->requiresPcs)
         > static_cast<int> (rhs->requiresPcs);
}

auto
findFirstFreeClassRoom (const std::vector<classRoom *> candidates) -> classRoom *
{
 
  classRoom *result = candidates[0];
  int first = result->freeHours[0];
  for (classRoom *classRoom : candidates)
    {
      int firstHour = classRoom->freeHours[0];
      if (firstHour < first)
        {
          result = classRoom;
          first = firstHour;
        }
    }

  return result;
}

auto
reserveClassRoom (exam *exam, classRoom *classRoom, int duration)
    -> std::vector<std::string>
{
  std::vector<std::string> result;

  int examStartingHour = classRoom->freeHours[0];
  int examEndingHour = examStartingHour + duration;

  if (examEndingHour <= 20)
    {
      result.push_back (exam->name);
      result.push_back (classRoom->id);
      std::string examDuration = std::to_string (examStartingHour) + "-"
                                 + std::to_string (examEndingHour);
      result.push_back (examDuration);
      result.push_back (std::to_string (classRoom->capacity));

      while (duration != 0)
        {
          classRoom->freeHours.erase (classRoom->freeHours.begin ());
          duration--;
        }

      if (exam->students > classRoom->capacity)
        {
          result.push_back (std::to_string (classRoom->capacity));
        }
      else
        {
          result.push_back (std::to_string (exam->students));
        }

      return result;
    }
  std::cout << "Exam: " + exam->name << " "
            << "cannot be organized today" << std::endl;
  return result;
}

void
populateHTML (const std::vector<std::vector<std::string> > &rows,
              const std::vector<std::string> &cancledExams,
              const std::string &outPath, const std::string &date)
{
  std::fstream file;
  file.open (outPath, std::ios::out);

  file << "<style>\n";
  file << "table, th, td {\n";
  file << "border: 1px solid black;\n";
  file << "border-collapse: collapse\n";
  file << "}\n";
  file << "th, td {\n";
  file << "padding: 10px;\n";
  file << "}\n";
  file << "</style>\n";
  file << "<table align = 'center'>\n";
  file << "<caption>Exam Schedule for " + date + "</caption>\n";
  file << "<tr>\n";

  file << "<td>Exam</td>\n";
  file << "<td>Class room</td>\n";
  file << "<td>Hours</td>\n";
  file << "<td>Class room capacity</td>\n";
  file << "<td>Number of students</td>\n";

  for (std::vector<std::string> row : rows)
    {
      file << "<tr>\n";

      file << "<td>" + row[0] + "</td>\n";
      file << "<td>" + row[1] + "</td>\n";
      file << "<td>" + row[2] + "</td>\n";
      file << "<td>" + row[3] + "</td>\n";
      file << "<td>" + row[4] + "</td>\n";

      file << "</tr>\n";
    }

  file << "</tr>\n";
  file << "</table>";
  file << "<br>\n";
  if (!cancledExams.empty ())
    {
      file << "<table align = 'center'>\n";
      file << "<tr>\n";
      file << "<td>Cancled Exams</td>\n";
      file << "</tr>\n";
      file << "<tr>\n";
      for (const std::string &ex : cancledExams)
        {
          file << "<td>" + ex + "</td>\n";
        }
      file << "</tr>\n";
      file << "</table>\n";
    }

  file.close ();
}

void
removeExam (std::string name, std::vector<std::vector<std::string> > &rows)
{
  rows.erase (std::remove_if (rows.begin (), rows.end (),
                              [&name] (const std::vector<std::string> &row) {
                                return row[0] == name;
                              }),
              rows.end ());
}

void
examScheduler::schedule (std::vector<exam *> exams,
                         const std::vector<classRoom *> &classRooms)
{
  std::vector<std::vector<std::string> > rows;
  std::vector<std::string> cancledExams;
  std::string date = exams[0]->date;
  std::string outPath = "../out/exams-" + date + "html";

  std::sort (exams.begin (), exams.end (), &comparator);
  
  std::vector<classRoom *> classRoomsWithPcs;
  std::copy_if (begin(classRooms), end(classRooms), std::back_inserter(classRoomsWithPcs),
                                                                [] (const classRoom* room){
                                                                    return room->hasPcs;
                                                                });

  for (exam *exam : exams)
    {
      int totalStudents = exam->students;

      while (totalStudents > 0)
        {
          classRoom *room
              = exam->requiresPcs ? 
              findFirstFreeClassRoom (classRoomsWithPcs):
              findFirstFreeClassRoom (classRooms);
          std::vector<std::string> row
              = reserveClassRoom (exam, room, exam->duration);
          if (row.empty ())
            {
              cancledExams.push_back (exam->name);
              removeExam (exam->name,
                          rows); // remove previous exam reservations if a new
                                 // room cannot be found
              break;
            }
          rows.push_back (row);
          exam->students -= room->capacity;
          totalStudents -= room->capacity;
        }
    }

  populateHTML (rows, cancledExams, outPath, date);
};
