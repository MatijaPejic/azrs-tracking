#ifndef __CLASS_ROOM__
#define __CLASS_ROOM__
#include <string>
#include <vector>

class classRoom
{
public:
  classRoom (std::string id, int capacity, bool hasPcs,
             std::vector<int> freeHours)
  {
    this->id = id;
    this->capacity = capacity;
    this->hasPcs = hasPcs;
    this->freeHours = freeHours;
  }
  std::string id;
  int capacity;
  bool hasPcs;
  std::vector<int> freeHours;
};
#endif
