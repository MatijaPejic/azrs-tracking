#ifndef __EXAM__
#define __EXAM__
#include <string>
class exam
{
public:
  exam (std::string name, std::string date, int students, int duration,
        bool requiresPcs)
  {
    this->name = name;
    this->date = date;
    this->students = students;
    this->duration = duration;
    this->requiresPcs = requiresPcs;
  }

  std::string name;
  std::string date;
  int students;
  int duration;
  bool requiresPcs;
};
#endif
