#include "parser.hpp"
#include <cstdlib>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

void
emptyCSV ()
{
  std::cerr << "Empty CSV file detected" << std::endl;
  abort ();
}

void
emptyLine ()
{
  std::cerr << "Empty line in CSV file detected" << std::endl;
  abort ();
}

auto
isEmpty (std::ifstream &fileName) -> bool
{
  return fileName.peek () == std::ifstream::traits_type::eof ();
}

auto
parser::split (const string &s, char delim) -> vector<string>
{
  vector<string> result;
  stringstream ss (s);
  string item;

  while (getline (ss, item, delim))
    {
      result.push_back (item);
    }

  return result;
};

auto
parser::parseClassRooms (const string &fileName) -> vector<classRoom *>
{
  vector<classRoom *> result;
  vector<int> freeHours = { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
  string line;
  ifstream input (fileName);

  if (isEmpty (input))
    {
      std::set_terminate (emptyCSV);
      throw -1;
    }

  if (input.is_open ())
    {
      while (getline (input, line))
        {
          if (line.empty ())
            {
              std::set_terminate (emptyLine);
              throw -1;
            }
          vector<string> tokens = split (line, ',');
          string id = tokens[0];
          int capacity = stoi (tokens[1]);
          bool hasPcs = stoi (tokens[2]) == 1;
          auto *room = new classRoom (id, capacity, hasPcs, freeHours);
          result.push_back (room);
        }

      input.close ();
    }
  else
    {
      cerr << "Unable to open file: " << fileName << endl;
    }

  return result;
};

auto
parser::parseDate (const string &fileName) -> std::string
{
  std::string dateParsed = split (fileName, '-').back ();
  vector<string> dateTokens = split (dateParsed, '.');
  dateTokens.pop_back ();
  string result;
  for (const string &token : dateTokens)
    {
      result += token;
      result += ".";
    }
  return result;
};

auto
parser::parseExams (const std::string &fileName) -> std::vector<exam *>
{
  std::string date = parseDate (fileName);
  vector<exam *> result;
  string line;
  ifstream input (fileName);

  if (isEmpty (input))
    {
      std::set_terminate (emptyCSV);
      throw -1;
    }

  if (input.is_open ())
    {
      while (getline (input, line))
        {
          if (line.empty ())
            {
              std::set_terminate (emptyLine);
              throw -1;
            }
          vector<string> tokens = split (line, ',');
          string id = tokens[0];
          int numOfStudents = stoi (tokens[1]);
          int duration = stoi (tokens[2]);
          bool requiresPcs = stoi (tokens[3]) == 1;
          exam *ex = new exam (id, date, numOfStudents, duration, requiresPcs);
          result.push_back (ex);
        }

      input.close ();
    }
  else
    {
      cerr << "Unable to open file: " << fileName << endl;
    }

  return result;
};
