#include <classRoom.hpp>
#include <exam.hpp>
#include <string>
#include <vector>

class examScheduler
{
public:
  examScheduler (){};

  static void schedule (std::vector<exam *> exams,
                        const std::vector<classRoom *> &classRooms);
};
