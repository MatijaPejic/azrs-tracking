# Exam Scheduler

Ideja projekta je napraviti scheduler za ispite na fakultetu.  
Kao ulaz aplikacija dobija dva csv-a gde prvi csv predstavlja ucionice a drugi ispite  
CSV za ucionice je organizovan po kolonama : id ucionice, kapacitet ucionice, da li ucionica ima racunare (0 ili 1)  
CSV za ispite je ogranizovan po kolonama : ime ispita, broj studenata, duzina isputa, da li ispit zahteva racunare  (0 ili 1)  
Kada scheduler zavrsi u direktorijumu out/ ce se naci html sa tabelom ispita.  
Ukoliko ne mogu svi studenti stati u jednu ucionicu scheduler ce dodeliti jos jednu ucionicu za taj ispit u istom vremenskom periodu ukoliko je to moguce.  
Ispiti koji zahtevaju racunar ce uvek biti u ucionici sa racunarom dok ispiti koji ne zahtevaju racunar ce biti smesteni u prvu slobodnu ucionicu.  
Scheduler nece zakazati ispit ukoliko bi se on zavrsio nakon 20h, najraniji ispit je u 8h. Ukoliko ispit ne moze biti organizovan tog dana u rezultujucem html-u ce biti dodata jos jedna tabela koja ce naznaciti da taj ispit nije mogao biti organizovan. 
Pokretanje aplikacije bez argumenata ce koristiti default-ne csv fajlove iz example_input direktorijuma.  
![Primer rezultujuce tabele](https://gitlab.com/MatijaPejic/azrs-tracking/-/blob/main/out/example_output.jpg)



