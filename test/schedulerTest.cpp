#include <classRoom.hpp>
#include <cstdio>
#include <exam.hpp>
#include <examScheduler.hpp>
#include <fstream>
#include <iostream>
#include <parser.hpp>
#include <string>
#include <vector>

auto
schedulerTest () -> int
{
  auto *p = new parser ();
  std::vector<exam *> exams
      = p->parseExams ("../test/validExams-1.1.2021.csv");
  std::vector<classRoom *> classRooms
      = p->parseClassRooms ("../test/validClassRoom.csv");

  auto *scheduler = new examScheduler ();

  // check if file exists before running scheduler and delete it
  if (FILE *file = fopen ("../out/exams-1.1.2021.html", "re"))
    {
      std::cout << "File exists before running scheduler, deleting ...\n";
      int status = std::remove ("../out/exams-1.1.2021.html");
      if (status == 0)
        {
          std::cout << "File deleted successfully\n";
        }
      else
        {
          std::cerr << "Couldn't delete file\n";
          return -1;
        }
    }

  scheduler->schedule (exams, classRooms);

  // check if file exists after running scheduler
  if (FILE *file = fopen ("../out/exams-1.1.2021.html", "re"))
    {
      fclose (file);
      return 0;
    }
  return -1;
}

auto
main () -> int
{
  if (schedulerTest () == -1)
    {
      return -1;
    }

  return 0;
}
