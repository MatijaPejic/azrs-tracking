#include <classRoom.hpp>
#include <utility>

#include <exam.hpp>
#include <examScheduler.hpp>
#include <iostream>
#include <parser.hpp>
#include <string>
#include <vector>

auto
splitTest (const std::string &s, char delim) -> int
{
  auto *p = new parser ();

  std::vector<std::string> result = p->split (s, delim);
  if (result.size () != 2)
    {
      return -1;
    }
  if (result[0] != "Hello" || result[1] != "World")
    {
      return -1;
    }

  return 0;
}

auto
parseDateTest (std::string fileName) -> int
{
  auto *p = new parser ();

  std::string result = p->parseDate (std::move (fileName));

  if (result != "1.1.2021.")
    {
      return -1;
    }

  return 0;
}

auto
parseValidExamsTest (std::string inputFile) -> int
{
  auto *p = new parser ();

  std::vector<exam *> result = p->parseExams (std::move (inputFile));

  if (result.size () != 12)
    {
      return -1;
    }

  return 0;
}

auto
parseEmptyLineExams (std::string inputFile) -> int
{
  auto *p = new parser ();

  try
    {
      p->parseExams (std::move (inputFile));
    }
  catch (int num)
    {
      return num;
    }

  return 0;
}

auto
parseEmptyCsv (std::string fileName) -> int
{
  auto *p = new parser ();
  try
    {
      p->parseExams (std::move (fileName));
    }
  catch (int num)
    {
      return num;
    }

  return 0;
}

auto
parseValidClassRoomCsv (std::string fileName) -> int
{
  auto *p = new parser ();

  std::vector<classRoom *> result = p->parseClassRooms (std::move (fileName));
  if (result.size () != 4)
    {
      return -1;
    }

  return 0;
}

auto
parseEmptyLineClassRoom (std::string fileName) -> int
{
  auto *p = new parser ();
  try
    {
      p->parseClassRooms (std::move (fileName));
    }
  catch (int num)
    {
      return num;
    }

  return 0;
}

auto
main () -> int
{
  // split test
  std::string input = "Hello.World";
  char delim = '.';
  if (splitTest (input, delim) == -1)
    {
      return -1;
    }

  // parse date test
  std::string inputDate = "testCSV-1.1.2021.csv";
  if (parseDateTest (inputDate) == -1)
    {
      return -1;
    }

  // parse empty csv
  std::string emptyCsv = "../test/empty.csv";
  if (parseEmptyCsv (emptyCsv) != -1)
    {
      return -1;
    }

  // parse valid exams
  std::string inputValidExams = "../test/validExams-1.1.2021.csv";
  if (parseValidExamsTest (inputValidExams) == -1)
    {
      return -1;
    }

  // parse empty line exams
  std::string inputEmpyLineExams = "../emptyLineExam.csv";
  if (parseEmptyLineExams (inputEmpyLineExams) != -1)
    {
      return -1;
    }

  // parse valid class rooms
  std::string validClassRooms = "../test/validClassRoom.csv";
  if (parseValidClassRoomCsv (validClassRooms) == -1)
    {
      return -1;
    }

  // parse empty line class rooms
  std::string emptyLineClassRoom = "../emptyLineClassRoom.csv";
  if (parseEmptyLineClassRoom (emptyLineClassRoom) != -1)
    {
      return -1;
    }

  return 0;
}
