#include <classRoom.hpp>
#include <exam.hpp>
#include <examScheduler.hpp>
#include <iostream>
#include <parser.hpp>
#include <vector>

auto
main (int argc, char *argv[]) -> int
{
  std::string default_example_class_rooms = "../example_input/classRooms.csv";
  std::string default_example_exams = "../example_input/exams-1.1.2021.csv";
  auto *p = new parser ();
  auto *scheduler = new examScheduler ();
  if (argc != 3)
    {
      std::cout << "Invalid arguments: Using example_input" << std::endl;
      std::cout << "Hint: Arguments classRoom.csv, exams-[exam_date].csv"
                << std::endl;
      std::vector<classRoom *> classRooms
          = p->parseClassRooms (default_example_class_rooms);
      std::vector<exam *> exams = p->parseExams (default_example_exams);
      scheduler->schedule (exams, classRooms);
    }
  else
    {
      std::cout << "Scheduling for : " << argv[1] << " " << argv[2]
                << std::endl;
      std::vector<classRoom *> classRooms = p->parseClassRooms (argv[1]);
      std::vector<exam *> exams = p->parseExams (argv[2]);
      scheduler->schedule (exams, classRooms);
    }

  return 0;
}
